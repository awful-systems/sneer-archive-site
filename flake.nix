{
  description = "A static site hosting the r/SneerClub archive";

  inputs = {
    flake-utils.url = "github:numtide/flake-utils";
    archive-data.url = "git://these.awful.systems/sneer-archive-data.git";
  };

  outputs = { self, nixpkgs, flake-utils, archive-data }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages."${system}";
      in {
        lib = pkgs.callPackage ./template.nix { };

        packages.site = let
          threads = (pkgs.lib.trivial.importJSON
            "${archive-data.packages."${system}".default}/threads-newest.json");
          bestest = (pkgs.lib.trivial.importJSON "${
              archive-data.packages."${system}".default
            }/submissions-bestest.json");
          longest = (pkgs.lib.trivial.importJSON "${
              archive-data.packages."${system}".default
            }/submissions-longest.json");
          newestPageFn = page: {
            prev = if page - 1 == 0 then
              "/archives"
            else
              "/archives/pages/${builtins.toString page}";
            next = "/archives/pages/${builtins.toString (page + 2)}";
            jump = n:
              if n == 0 then
                "/archives"
              else
                "/archives/pages/${builtins.toString (n + 1)}";
          };
          bestestPageFn = page: {
            prev = if page - 1 == 0 then
              "/archives/bestest"
            else
              "/archives/bestest/${builtins.toString page}";
            next = "/archives/bestest/${builtins.toString (page + 2)}";
            jump = n:
              if n == 0 then
                "/archives/bestest"
              else
                "/archives/bestest/${builtins.toString (n + 1)}";
          };
          longestPageFn = page: {
            prev = if page - 1 == 0 then
              "/archives/longest"
            else
              "/archives/longest/${builtins.toString page}";
            next = "/archives/longest/${builtins.toString (page + 2)}";
            jump = n:
              if n == 0 then
                "/archives/longest"
              else
                "/archives/longest/${builtins.toString (n + 1)}";
          };
          siteName = "r/SneerClub archives";
          numPerPage = 25;
          templates =
            self.lib."${system}".mkTemplates "sneer-archive" ./templates [
              "base.html"
              "thread.html"
              "comment.html"
              "submission-page.html"
              "pagination.html"
              "topbar.html"
              "search.html"
            ];
          baseWithSort = title: sort: content:
            templates "base.html" {
              inherit title sort content;
              topbarTpl = templates "topbar.html";
            };
          base = title: content: baseWithSort title null content;
          paginationTpl = templates "pagination.html";
          submissionTpl = templates "submission-page.html";
          topbarTpl = templates "topbar.html";
          newestPages = builtins.map (n: {
            inherit n;
            content = baseWithSort
              "${siteName} &mdash; page ${builtins.toString (n + 1)}" "newest"
              (submissionTpl {
                inherit threads numPerPage topbarTpl paginationTpl;
                title = "${siteName} &mdash; page ${builtins.toString (n + 1)}";
                page = n;
                pageFn = newestPageFn n;
              });
          }) (pkgs.lib.lists.range 1
            (builtins.floor (builtins.length threads / numPerPage)));
          bestestPages = builtins.map (n: {
            inherit n;
            content = baseWithSort
              "${siteName} &mdash; page ${builtins.toString (n + 1)}" "bestest"
              (submissionTpl {
                inherit numPerPage topbarTpl paginationTpl;
                threads = bestest;
                title = "${siteName} &mdash; page ${builtins.toString (n + 1)}";
                page = n;
                pageFn = bestestPageFn n;
              });
          }) (pkgs.lib.lists.range 1
            (builtins.floor (builtins.length bestest / numPerPage)));
          longestPages = builtins.map (n: {
            inherit n;
            content = baseWithSort
              "${siteName} &mdash; page ${builtins.toString (n + 1)}" "longest"
              (submissionTpl {
                inherit numPerPage topbarTpl paginationTpl;
                threads = longest;
                title = "${siteName} &mdash; page ${builtins.toString (n + 1)}";
                page = n;
                pageFn = longestPageFn n;
              });
          }) (pkgs.lib.lists.range 1
            (builtins.floor (builtins.length longest / numPerPage)));
          threadPages = builtins.map (thread: {
            id = thread.id;
            content = base "${thread.title} &mdash; ${siteName}"
              (templates "thread.html" {
                inherit thread topbarTpl;
                commentTpl = templates "comment.html";
              });
          }) threads;
          searchResults = base "Search results &mdash; ${siteName}"
            (templates "search.html" { });
        in pkgs.runCommand "generate-site" { } ''
          mkdir -p $out/archives
          mkdir -p $out/archives/thread
          mkdir -p $out/archives/pages
          mkdir -p $out/archives/bestest
          mkdir -p $out/archives/longest

          cat <<'__EOF__' > $out/archives/index.html
          ${baseWithSort siteName "newest" (submissionTpl {
            inherit threads numPerPage topbarTpl paginationTpl;
            title = siteName;
            page = 0;
            pageFn = newestPageFn 0;
          })}
          __EOF__
          ${builtins.concatStringsSep "\n" (builtins.map (page: ''
            cat <<'__EOF__' > $out/archives/pages/${
              builtins.toString (page.n + 1)
            }.html
            ${page.content}
            __EOF__
          '') newestPages)}

          cat <<'__EOF__' > $out/archives//bestest.html
          ${baseWithSort siteName "bestest" (submissionTpl {
            inherit numPerPage topbarTpl paginationTpl;
            title = siteName;
            threads = bestest;
            page = 0;
            pageFn = bestestPageFn 0;
          })}
          __EOF__
          ${builtins.concatStringsSep "\n" (builtins.map (page: ''
            cat <<'__EOF__' > $out/archives/bestest/${
              builtins.toString (page.n + 1)
            }.html
            ${page.content}
            __EOF__
          '') bestestPages)}

          cat <<'__EOF__' > $out/archives/longest.html
          ${baseWithSort siteName "longest" (submissionTpl {
            inherit numPerPage topbarTpl paginationTpl;
            title = siteName;
            threads = longest;
            page = 0;
            pageFn = longestPageFn 0;
          })}
          __EOF__
          ${builtins.concatStringsSep "\n" (builtins.map (page: ''
            cat <<'__EOF__' > $out/archives/longest/${
              builtins.toString (page.n + 1)
            }.html
            ${page.content}
            __EOF__
          '') longestPages)}

          ${builtins.concatStringsSep "\n" (builtins.map (page: ''
            cat <<'__EOF__' > $out/archives/thread/${page.id}.html
            ${page.content}
            __EOF__
          '') threadPages)}

          cat <<'__EOF__' > $out/archives/search.html
          ${searchResults}
          __EOF__

          cp ${
            archive-data.packages."${system}".default
          }/submissions-bestest.json $out/archives/lunr.json

          if [ -n "$(ls -A ${./static} 2>/dev/null)" ]; then cp -R ${
            ./static
          }/* $out/archives; fi
        '';

        packages.serve = let
        in pkgs.writeShellScriptBin "serve" ''
          shopt -s globstar
          while true; do
                ls -d templates/* static/* **/*.nix flake.lock | ${pkgs.entr}/bin/entr -r -d bash -c "\
                   /usr/bin/env time -f 'Nix rebuild finished in %es'\
                   nix build $1 --show-trace && \
                   ${pkgs.caddy}/bin/caddy run --config ${
                     ./Caddyfile
                   } --adapter caddyfile"
                if [ $? -eq 0 ]; then break; fi
          done
        '';

        packages.default = self.packages."${system}".site;

        devShells.default = pkgs.mkShell {
          buildInputs = [
            self.packages."${system}".serve
            pkgs.html-tidy
            archive-data.packages."${system}".default # cache archive data
          ];
        };
      });
}
