{ lib, pkgs, ... }:

let
  templateLib = lib // {
    map = f: list: builtins.concatStringsSep "" (builtins.map f list);
  };
in {
  mkTemplates = name: templatesDir: templates:
    let
      templatesDrv = pkgs.runCommand "${name}-templates" { } ''
        shopt -s globstar
        for t in ${templatesDir}/**/*; do
            subdir=$(dirname "$t" | cut -d'/' -f5-)
            name=$(basename "$t")
            echo $subdir

            mkdir -p "$out/$subdir"
            [ -f "$t" ] &&
            echo "{lib, ...}@args: '''" | cat - "$t" > "$out/$subdir/$name.nix" &&
            echo "'''" >> "$out/$subdir/$name.nix"
        done
      '';
      templateCache = builtins.listToAttrs (builtins.map (t: {
        name = t;
        value = (import "${templatesDrv}/${t}.nix");
      }) templates);
    in template: args:
    ((builtins.getAttr template templateCache)
      ({ lib = templateLib; } // args));
}
