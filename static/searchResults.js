function el(element, config) {
  const e = document.createElement(element);

  if(config) {
    if(config.children) {
      for(const child of config.children.filter(a => a)) {
        e.appendChild(child);
      }

      delete config.children;
    }

    for(const key in config) {
      e[key] = config[key];
    }
  }

  return e;
}

async function setup_lunr() {
  const index_json = await (await fetch('/archives/lunr.json')).json();
  const num_docs = index_json.length;
  const search_index = elasticlunr(function() {
    this.addField('title');
    this.addField('author');
    this.setRef('id');
  });

  for(const doc of index_json) {
    search_index.addDoc(doc);
  }

  return [search_index, num_docs];
}

setup_lunr().then(([index, num_docs]) => {
  const params = new URLSearchParams(window.location.search);
  const query = params.get('query');
  const submissionList = document.getElementById('searchResults');
  const results = index.search(query, {});
  submissionList.innerHTML = '';
  submissionList.className = 'submissionList';
  submissionList.appendChild(el('div', {
    className: 'submissionItem',
    children: [
      el('span', {
        textContent: `${results.length} results for "${query}" (out of ${num_docs} archived posts)`
      })
    ]
  }));

  for(const r of results) {
    submissionList.appendChild(el('div', {
      className: 'submissionItem',
      children: [
        el('div', {
          className: 'score',
          textContent: r.doc.score
        }),
        el('div', {
          className: 'submissionContent',
          children: [
            el('div', {
              className: 'title',
              children: [
                el('a', {
                  href: `/archives/thread/${r.doc.id}`,
                  children: [
                    r.doc.link_flair_text &&
                      el('span', {
                        className: 'flair',
                        textContent: `${r.doc.link_flair_text} `
                      }),
                    el('span', {
                      textContent: r.doc.title
                    })
                  ]
                }),
                r.doc.url && el('span', {
                  className: 'url',
                  children: [
                    el('a', {
                      href: r.doc.url,
                      innerHTML: ` <span class="truncate">${r.doc.url}</span>`
                    })
                  ]
                })
              ]
            }),
            el('div', {
              className: 'subtitle',
              children: [
                el('span', {
                  textContent: `posted at ${r.doc.created_date} by `
                }),
                el('span', {
                  className: 'author',
                  textContent: `u/${r.doc.author}`
                })
              ]
            }),
            el('div', {
              className: 'subtitle',
              children: [
                el('a', {
                  href: `/archives/thread/${r.doc.id}`,
                  textContent: `${r.doc.num_comments} comments`
                })
              ]
            })
          ]
        })
      ]
    }));
  }
});
